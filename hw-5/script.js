//не впевнена, що картка зверстана корректно,так як приклад картки був видалений

const postContainer = document.querySelector('.post-container'); // Отримуємо елемент контейнера для постів з DOM
const modal = document.querySelector('#modal'); // Отримуємо модальне вікно з DOM
const modalUpdate = document.querySelector('#modal-update'); // Отримуємо модальне вікно з DOM
const btnShowModal = document.querySelector('#openModal'); // Кнопка для відображення модального вікна
const btnCloseModal = document.querySelector('.close'); // Кнопка для закриття модального вікна
const openModalButton = document.querySelector('#openModal'); // Кнопка відкриття модального вікна
const addPostButton = document.querySelector('#addPost'); // Кнопка додавання посту
const updatePostButton = document.querySelector('#updatePost'); // Кнопка оновлення посту
const titleInput = document.querySelector('#title'); // Поле введення заголовку посту
const contentInput = document.querySelector('#content'); // Поле введення вмісту посту
const titleInputUpdate = document.querySelector('#titleUpdate'); // Поле введення оновленого заголовку
const contentInputUpdate = document.querySelector('#contentUpdate'); // Поле введення оновленого вмісту

let myUsers; // Змінна для збереження завантажених користувачів

class Card {
    constructor(title, content, name, email, id) {
        this.title = title;
        this.content = content;
        this.name = name;
        this.email = email;
        this.id = id;
        this.element = document.createElement('div'); // Створюємо новий елемент div
    }

    removeNode() {
        postContainer.removeChild(this.element); // Видаляємо елемент з DOM
        console.log("Post with id " + this.id + " deleted!");
    }

    async onDelete() {
        await fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
            method: "DELETE"
        }).then(({ status, ok }) => {
            if (status === 200 && ok) {
                this.removeNode(); // Викликаємо метод видалення, якщо запит успішний
            }
        })
    }

    async onUpdate() {
        await fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
            method: "GET"
        }).then(data => data.json()).then(({ title, body, userId, id }) => {

            modalUpdate.classList.add('show');
            titleInputUpdate.value = title;
            contentInputUpdate.value = body;
            updatePostButton.setAttribute('data-id', id);
            updatePostButton.setAttribute('data-userid', userId);

        })
    }

    render() {
        const card = `
            <div class="post__author">
                <p>${this.name}</p>
                <p>${this.email}</p> 
            </div>
            <h2 class="post-title">${this.title}</h2>
            <p class="post-body">${this.content}</p>
            <div class="post__buttons">
                <button class="update">UPDATE</button>
                <button class="delete">DELETE</button>
            </div>
            `.trim(); // Генеруємо HTML-код карточки

        this.element.classList.add('post'); // Додаємо клас 'post' до елементу        this.element.classList.add('post'); // Додаємо клас 'post' до елементу
        this.element.classList.add('post-' + this.id); // Додаємо клас 'post' до елементу        this.element.classList.add('post'); // Додаємо клас 'post' до елементу
        this.element.innerHTML = card; // Встановлюємо HTML-код карточки в елемент

        this.element.querySelector('.delete').addEventListener('click', () => this.onDelete()); // Додаємо обробник події для кнопки видалення
        this.element.querySelector('.update').addEventListener('click', () => this.onUpdate()); // Додаємо обробник події для кнопки оновлення
        postContainer.insertAdjacentElement("afterbegin", this.element); // Додаємо елемент в контейнер з постами
    }
}

// Оголошення констант для URL-адрес користувачів та постів
const API_URL_USERS = 'https://ajax.test-danit.com/api/json/users';
const API_URL_POSTS = 'https://ajax.test-danit.com/api/json/posts';

// Функція для отримання даних з сервера
const fetchData = async () => {
    // Створення обіцянок для завантаження даних про користувачів та пости
    const userPromise = fetch(API_URL_USERS).then((data) => data.json());
    const postsPromise = fetch(API_URL_POSTS).then((data) => data.json());

    // Використання Promise.allSettled для одночасного чекання обіцянок
    Promise.allSettled([userPromise, postsPromise]).then((data) => {
        // Відфільтровуємо результати на основі статусу виконання
        const fulfilledData = data.filter((d) => d.status === 'fulfilled').map(d => d.value);
        const [users, posts] = fulfilledData; // Розпакування результатів

        myUsers = users; // Зберігаємо користувачів у змінну myUsers

        if (users && posts) {
            posts.forEach(post => {
                const { userId } = post;
                const user = users.find((u) => u.id === userId);

                // Додаємо пости до відповідних користувачів
                user.posts = user.posts ? [...user.posts, post] : [post];
            });

            users.forEach(({ name, email, posts }) => {
                posts.forEach(({ title, body, id }) => {
                    const postCard = new Card(title, body, name, email, id);

                    postCard.render(); // Відображення карти поста
                })
            })
        }
    }).finally(() => {
        document.querySelector('.overlay').style.display = 'none'; // Приховання оверлею після завантаження даних
    })
}

fetchData(); // Виклик функції отримання даних


// Функція для додавання поста
const addPost = async (post) => {
    fetch('https://ajax.test-danit.com/api/json/posts', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(post)
    }).then((res) => res.json()).then((post) => {
        const { title, body, name, email, id } = post;

        const postCard = new Card(title, body, name, email, id);
        postCard.render(); // Відображення карти нового поста

        titleInput.value = ''; // Очищення поля для введення заголовка
        contentInput.value = ''; // Очищення поля для введення вмісту
    })
}

// Обробник події для натискання кнопки "Додати пост"
addPostButton.addEventListener('click', () => {
    if (myUsers) { // Перевірка, чи є завантажені користувачі
        const user = myUsers[0]; // Вибираємо першого користувача (якщо вони завантажені)
        const post = {
            name: user.name,
            title: titleInput.value,
            body: contentInput.value,
            email: user.email,
        }; // Створення об'єкту поста
        addPost(post); // Виклик функції для додавання поста
        modal.classList.remove('show'); // Видалення класу 'show' для закриття модального вікна
    }
})


// Функція для оновлення поста
const updatePost = async (post, id) => {
    fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(post)
    }).then((res) => res.json()).then((post) => {
        const { title, body, id } = post;

        const divPost = document.querySelector('.post-' + id);
        divPost.querySelector('.post-title').innerHTML = title;
        divPost.querySelector('.post-body').innerHTML = body;
    })
}

// Обробник події для натискання кнопки "Оновити пост"
updatePostButton.addEventListener('click', () => {
    const id = updatePostButton.getAttribute('data-id');
    const post = {
        title: titleInputUpdate.value,
        body: contentInputUpdate.value
    }; // Створення об'єкту поста
    updatePost(post, id); // Виклик функції для додавання поста
    modalUpdate.classList.remove('show'); // Видалення класу 'show' для закриття модального вікна
})

// Обробник події для натискання кнопки відкриття модального вікна
btnShowModal.addEventListener('click', () => {
    modal.classList.toggle('show'); // Додавання/видалення класу 'show' для відображення/приховання модального вікна
})

// Обробник події для натискання кнопки закриття модального вікна
btnCloseModal.addEventListener('click', () => {
    modal.classList.remove('show'); // Видалення класу 'show' для закриття модального вікна
})

// Обробник події для натискання кнопки закриття модального вікна
modalUpdate.querySelector('.close').addEventListener('click', () => {
    modalUpdate.classList.remove('show'); // Видалення класу 'show' для закриття модального вікна
})
