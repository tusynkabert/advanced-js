// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// технологія яка дозволяє відправляти та отримувати дані без перезавантаження сторінки.

// загальна функція для отримання даних
function getData(url) {
    return fetch(url).then(res => res.json());
}

//головний тег для виводу даних
const root = document.querySelector("#root");

//виводимо повідомлення завантаження фільмів
root.innerHTML = '<div class="preloader"><span></span><span></span><span></span><span></span></div>';

// отримуємо данні фільму
getData('https://ajax.test-danit.com/api/swapi/films')
    .then((filmList) => {

        //очищаємо текст завантаження
        root.innerHTML = '';

        //виводимо дані на екран
        filmList.forEach(
            async ({ episodeId, name, openingCrawl, characters }) => {

                // Виводмо дані про фільм
                root.insertAdjacentHTML("afterbegin",
                    `<div class="film-holder">
                        <div class="film-info">
                            <h1 class="film-title">
                                ${name}
                                <span class="film-number">Епізод: #${episodeId}</span>
                            </h1>
                            <p class="film-description">${openingCrawl}</p>
                        </div>
                
                        <h2>Актори:</h2>
                        <ul class="actors-list">
                            ${await getCharacters(characters)}
                        </ul>
                    </div>`)
            }
        );
    })

// функція для отримання персонажів
async function getCharacters(characters) {

    //масив длдя збору промісів
    const arrUlrs = [];

    //збираємо в єдину змінну
    let htmlLi = '';

    // перебераємо масив посилань персонажів
    characters.forEach(url => {

        //добавляємо до масиву
        arrUlrs.push(getData(url))

    });

    //чекаємо поки масив сформує результат 
    const results = await Promise.all(arrUlrs);

    //перебираємо масив відповідей
    results.forEach(({ name }) => {


        htmlLi += `<li>${name}</li>`;

    });

    return htmlLi;
}