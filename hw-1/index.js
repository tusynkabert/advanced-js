//1. функції відносно типу данних, які можна дописувати.
//2. щоб отриматі дані з попереднього класу




class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get getName() {
        return this.name;
    }

    set setName(name) {
        this.name = name;
    }

    get getAge() {
        return this.age;
    }

    set setAge(age) {
        this.age = age;
    }

    get getSalary() {
        return this.salary;
    }

    set setSalary(salary) {
        this.salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get getLang() {
        return this.lang;
    }

    set setLang(lang) {
        this.lang = lang;
    }

    get getSalary() {
        return this.salary * 3;
    }
}



const programmer1 = new Programmer("Kenzie", 25, 50000, ["JavaScript", "Php"]);
const programmer2 = new Programmer("Weslie", 30, 60000, ["JavaScript", "React"]);

console.log(programmer1.getName); // Виведе: Kenzie
console.log(programmer1.getAge); // Виведе: 25
console.log(programmer1.getSalary); // Виведе: 150000
console.log(programmer1.getLang); // Виведе: [ 'JavaScript', 'Php' ]

console.log(programmer2.getName); // Виведе: Weslie
console.log(programmer2.getAge); // Виведе: 30
console.log(programmer2.getSalary); // Виведе: 180000
console.log(programmer2.getLang); // Виведе: [ 'JavaScript', 'React' ]
