// дозволяє виконувати оперції паралельно.


//(в задачі некоректно відображає локацію за ІР адресою, допускаю,що на це впливає локація інтернет провайдера,але не впевнена. як це виправити не знаю)


const getIpButton = document.querySelector('#getIp'); // Кнопка для отримання IP-адреси та інформації
const userInfoList = document.querySelector('#userInfo'); // Список для відображення інформації про користувача

// URL для отримання IP-адреси та URL для отримання інформації про користувача за IP-адресою
const GET_IP_URL = 'https://api.ipify.org/?format=json'; // URL для отримання IP-адреси
const getUserInfoUrl = (ip) => `http://ip-api.com/json/${ip}?fields=district,continent,country,regionName,city`; // Генерація URL для отримання інформації про користувача

// Функція для відображення інформації про користувача
const drawUserInfo = (dataObj) => {
    userInfoList.innerHTML = ''; // Очищення списку перед відображенням нової інформації

    // Перебираємо властивості об'єкта та додаємо їх до списку як пункти
    Object.entries(dataObj).map(([key, value]) => {
        const li = document.createElement('li'); // Створюємо новий елемент списку
        li.textContent = `${capitalize(key)} - ${value || 'No data provided'}`; // Встановлюємо текст для пункту списку
        userInfoList.appendChild(li); // Додаємо пункт до списку
    });
}

// Функція для отримання та відображення інформації про користувача за IP-адресою
const getUserInfoFromIp = async (ip) => {
    const data = await fetch(getUserInfoUrl(ip)).then((res) => res.json()); // Отримання даних та їх перетворення в об'єкт

    drawUserInfo(data); // Виклик функції для відображення інформації
}

// Функція для отримання IP-адреси та відображення інформації про користувача
const getUserIp = async () => {
    const { ip } = await fetch(GET_IP_URL).then((res) => res.json()); // Отримання IP-адреси

    getUserInfoFromIp(ip); // Виклик функції для отримання та відображення інформації
}

// Додавання обробника події для кнопки отримання IP-адреси
getIpButton.addEventListener('click', () => {
    getUserIp(); // Виклик функції отримання IP-адреси та інформації про користувача
})

// Функція для першої букви слова у верхньому регістрі
function capitalize(s) {
    return s[0].toUpperCase() + s.slice(1);
}

