// зручно використовувати для перехоплення неправильної поведінки, яка викликає помилку.


const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


function isExist(data) {
    return typeof data != 'undefined';
}

const root = document.getElementById('root');


let list = '<ul>';

books.forEach(
    function ({ author, name, price }, keyArray) {

        try {
            if (!isExist(author))
                throw (`object: ${keyArray}, author not exist!`);

            if (!isExist(name))
                throw (`object: ${keyArray}, name not exist!`);

            if (!isExist(price))
                throw (`object: ${keyArray}, price not exist!`);

            if (
                isExist(author) &&
                isExist(name) &&
                isExist(price)
            ) {
                list = list + `<li>author: ${author}, name: "${name}", price: ₴${price}</li>`;
            }
        }
        catch (error) {
            console.log(error);
        }

    }
);

list = list + '</ul>';

root.innerHTML = list;


